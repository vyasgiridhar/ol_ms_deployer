import yaml

from task import Task

class Job:
    """Job is a contaner for the Concourse Job"""

    __slots__ = ['content', 'name', 'serial', 'tasks']

    def __init__(self, name, serial = True):
        self.content = None
        self.name = name
        self.serial = True
        self.tasks = []

    def load_from_file(self, file_path) -> 'Job':
        "Load the Job definition from the given yaml file path"
        with open(file_path) as file:
            # The FullLoader parameter handles the conversion from YAML
            # scalar values to Python the dictionary format
            self.content = yaml.load(file, Loader=yaml.FullLoader)
            print(f"Loaded Job definition from {file_path}")
        
        return self

    def add_task(self, task: Task) -> 'Job':
        self.tasks.append(task)
        return self