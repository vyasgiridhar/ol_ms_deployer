from job import Job
from typing import List

class Group:
    """Grouping Jobs based on a name"""

    def __init__(self, name: str, jobs: List[Job]):
        self.name = name
        self.jobs = jobs
    
    def clone_jobs(self, name: str) -> 'Group':
        group = Group(name, self.jobs)
        return group


class Groups:
    "Group of Groups"
    def __init__(self, groups: List[Group]):
        self.groups = groups
