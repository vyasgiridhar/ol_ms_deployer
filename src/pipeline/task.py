import yaml


class Task:
    """A Task contains platform, inputs, outputs, caches, params, run.
       Use them wisely
    """

    def __init__(self, content: str):
        self.content = content

    def load_from_file(self, file_path) -> 'Task':
        "Load the Task from the given yaml file path"
        with open(file_path) as file:
            # The FullLoader parameter handles the conversion from YAML
            # scalar values to Python the dictionary format
            self.content = yaml.load(file, Loader=yaml.FullLoader)
            print(f"Loaded Task definition from {file_path}")
        
        return self