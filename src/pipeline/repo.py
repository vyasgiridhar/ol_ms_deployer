import yaml


class Repository:
    """Repository is a contaner for the Concourse git resource"""
    name = ""
    repository_type = ""

    def __init__(self, name: str):
        self.content = None
        self.name = name

    def load_from_file(self, file_path) -> 'Repository':
        "Load the Repository from the given yaml file path"
        with open(file_path) as file:
            # The FullLoader parameter handles the conversion from YAML
            # scalar values to Python the dictionary format
            self.content = yaml.load(file, Loader=yaml.FullLoader)
            print(f"Loaded repository definition from {file_path}")
        
        return self